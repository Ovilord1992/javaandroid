package com.example.test8;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    double lat,lon;
    ImageButton mapView,btnGetLoc,getWay,car,mapTopmap,sputnik;

    private View mapViews;

    List<LatLng> latLngList = new ArrayList<>();
    List<Marker> markerList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        //Кнопка запоминания позиции автомобиля
        sputnik = (ImageButton) findViewById(R.id.sputnik);
        mapTopmap = (ImageButton) findViewById(R.id.mapTopmap);
        btnGetLoc = (ImageButton) findViewById(R.id.btnGetLoc);
        car = (ImageButton) findViewById(R.id.car);
        mapView = (ImageButton) findViewById(R.id.mapView);
        getWay = (ImageButton) findViewById(R.id.getWay);
        ActivityCompat.requestPermissions(MapsActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);


        btnGetLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPStracer g = new GPStracer(getApplicationContext());
                Location l = g.getLocation();


                //проверка доступность координат
                if (l != null){

                    //получаем данные долготы и широты
                    lat = l.getLatitude();
                    lon = l.getLongitude();

                    MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icred)).draggable(true);
                    Marker marker = mMap.addMarker(markerOptions);
                    latLngList.add(new LatLng(lat, lon));
                    markerList.add(marker);


                    //приближение камеры с анимацией
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lon)).zoom(22).bearing(45).tilt(20).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    mMap.animateCamera(cameraUpdate);


                }else{

                    //Вывод сообщения об отсутствии сигнала
                    Toast.makeText(getApplicationContext(),"GPS сигнал отсутствует", Toast.LENGTH_LONG).show();
                }
            }
        });


        //switch map/sputnik
        mapTopmap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            }

        });

        sputnik.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

            }

        });


        car.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String catName = "";
                for (LatLng name : latLngList) {
                    catName = catName + name + " ";
                    if (catName != null) {
                        LatLng ls = latLngList.get(0);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(ls).zoom(22).bearing(45).tilt(20).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        mMap.animateCamera(cameraUpdate);
                    }
                }
            }

        });


        //перенаправление камеры на авто
        getWay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //вывод диалогового окна подтверждения
                //Alert
                AlertDialog.Builder builder= new AlertDialog.Builder(MapsActivity.this);
                builder.setMessage("Удалить все сохраненные позиции?").setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Очиска всех маркеров
                        for (Marker marker : markerList) marker.remove();
                        latLngList.clear();
                        markerList.clear();

                    }
                }).setNegativeButton("Нет",null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapTop);
        mapFragment.getMapAsync(this);
        mapViews = mapFragment.getView();
    }


    /*@Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(sydney).title("you this").draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    } */


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        GPStracer g = new GPStracer(getApplicationContext());
        Location l = g.getLocation();
        LatLng myPos = new LatLng(l.getLatitude(), l.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPos, 18));



        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.icred)).draggable(true);
                Marker marker = mMap.addMarker(markerOptions);
                latLngList.add(latLng);
                markerList.add(marker);
            }

        });

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //переопределение положения кнопки навигации
        if (mapViews != null && mapViews.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapViews.findViewById(Integer.parseInt("1")).getParent())
                    .findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 750);
        }
        //переопределение положения зума
        if (mapViews != null && mapViews.findViewById(Integer.parseInt("1")) != null) {
            View zoomIn = ((View) mapViews.findViewById(Integer.parseInt("1")).getParent())
                    .findViewById(Integer.parseInt("1"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) zoomIn.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 300);
        }
    }

}
